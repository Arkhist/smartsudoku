package fr.arkhist.smartsudoku;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Chronometer;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import fr.arkhist.smartsudoku.sudoku.SudokuGenerator;

public class SudokuActivity extends AppCompatActivity {

    public static final String SAVE_NAME = "sudokuSave";

    private final Map<Integer, TextView> numberButtonMap = new HashMap<Integer, TextView>();
    private TextView numberDelete;
    private Chronometer simpleChronometer;

    private AlertDialog winDialog;

    private final TextView[][] sudokuGridText = new TextView[9][9];
    private byte[][] sudokuGridNum = new byte[][]{
            {9, 7, 3, 6, 2, 4, 5, 1, 8},
            {6, 8, 4, 5, 1, 9, 7, 2, 3},
            {2, 1, 5, 3, 8, 7, 9, 4, 6},
            {1, 5, 9, 7, 4, 3, 8, 6, 2},
            {7, 6, 8, 2, 5, 1, 3, 9, 4},
            {4, 3, 2, 8, 9, 6, 1, 5, 7},
            {8, 9, 1, 4, 3, 2, 6, 7, 5},
            {5, 4, 7, 1, 6, 8, 2, 3, 9},
            {3, 2, 6, 9, 7, 5, 4, 0, 1},
    };

    private byte selectedCellX = -1;
    private byte selectedCellY = -1;

    public enum SudokuDifficulty {
        EASY(0.60f, "Easy", 0),
        MEDIUM(0.75f, "Medium", 1),
        HARD(1.00f, "Hard", 2);

        public final float triesPercent;
        public final String displayName;
        public final int saveIndex;

        SudokuDifficulty(float triesPercent, String displayName, int saveIndex) {
            this.triesPercent = triesPercent;
            this.displayName = displayName;
            this.saveIndex = saveIndex;
        }
    }

    private SudokuDifficulty currentDifficulty = SudokuDifficulty.HARD;

    public static void deleteSave(AppCompatActivity activity) {
        activity.deleteFile(SAVE_NAME);
    }

    public static boolean saveExists(AppCompatActivity activity) {
        File save = new File(activity.getApplicationContext().getFilesDir(), SAVE_NAME);
        return save.exists();
    }

    private void saveGrid() {
        ByteBuffer saveBuffer = this.serialize();
        try {
            FileOutputStream file = openFileOutput(SAVE_NAME, MODE_PRIVATE);
            FileChannel fileChannel = file.getChannel();
            fileChannel.write(saveBuffer);
            file.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.item1: {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                return true;
            }
            case R.id.item2: {
                SudokuActivity.deleteSave(this);
                Intent intent2 = new Intent(getApplicationContext(), SudokuActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent2);
                finish();
                return true;
            }
            case R.id.item3: {
                this.saveGrid();
                Toast.makeText(getApplicationContext(), R.string.saved, Toast.LENGTH_SHORT).show();
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sudoku);

        numberButtonMap.put(1, (TextView) findViewById(R.id.number1));
        numberButtonMap.put(2, (TextView) findViewById(R.id.number2));
        numberButtonMap.put(3, (TextView) findViewById(R.id.number3));
        numberButtonMap.put(4, (TextView) findViewById(R.id.number4));
        numberButtonMap.put(5, (TextView) findViewById(R.id.number5));
        numberButtonMap.put(6, (TextView) findViewById(R.id.number6));
        numberButtonMap.put(7, (TextView) findViewById(R.id.number7));
        numberButtonMap.put(8, (TextView) findViewById(R.id.number8));
        numberButtonMap.put(9, (TextView) findViewById(R.id.number9));

        simpleChronometer = findViewById(R.id.simpleChronometer);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String level = extras.getString("level");
            this.currentDifficulty = SudokuDifficulty.valueOf(level);
        }

        TextView difficultyText = findViewById(R.id.level);
        difficultyText.setText(this.currentDifficulty.displayName);
        difficultyText.setVisibility(View.VISIBLE);

        for(int i = 1; i < 10; i++) {
            final int finalI = i;
            numberButtonMap.get(i).setOnClickListener((view) -> {
                this.setNum((byte) finalI);
            });
        }
        numberDelete = findViewById(R.id.numberx);
        numberDelete.setOnClickListener((view) -> {
            this.deleteNum();
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.restart_query)
                .setTitle(R.string.congratulations);

        builder.setPositiveButton(R.string.restart, (dialog, id) -> {
            SudokuActivity.deleteSave(this);
            Intent intent = new Intent(getApplicationContext(), SudokuActivity.class)
                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("level", this.currentDifficulty.name());
            startActivity(intent);
            finish();
        });
        builder.setNegativeButton(R.string.menu, (dialog, id) -> {
            Intent intent = new Intent(getApplicationContext(), MainActivity.class)
                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        });
        winDialog = builder.create();

        loadSudokuGrid();
        simpleChronometer.start();
    }

    private void deleteNum() {
        this.setCell(this.selectedCellY, this.selectedCellX, (byte) 0);
        updateCellChoices();
    }

    private void setNum(byte value) {
        this.setCell(this.selectedCellY, this.selectedCellX, value);
        updateCellChoices();
    }

    private void deselectCell() {
        if (selectedCellX != -1 && selectedCellY != -1) {
            sudokuGridText[selectedCellY][selectedCellX]
                    .setBackgroundResource(R.drawable.sudoku_cell_border);
        }
        selectedCellX = -1;
        selectedCellY = -1;

        for (int i = 1; i < 10; i++) {
            numberButtonMap.get(i).setVisibility(TextView.GONE);
        }
        numberDelete.setVisibility(TextView.GONE);
    }

    private void selectCell(final byte y, final byte x) {
        deselectCell();
        if (sudokuGridNum[y][x] > 0)
            return;
        selectedCellX = x;
        selectedCellY = y;

        updateCellChoices();

        sudokuGridText[y][x].setBackgroundResource(R.drawable.sudoku_cell_selected);
    }

    private void updateCellChoices() {
        int x = selectedCellX;
        int y = selectedCellY;
        int squareX = x / 3;
        int squareY = y / 3;

        Set<Integer> possible = new HashSet<Integer>();
        for (int i = 1; i < 10; i++)
            possible.add(i);

        // Square check
        for (int ys = squareY * 3; ys < squareY * 3 + 3; ys++)
            for (int xs = squareX * 3; xs < squareX * 3 + 3; xs++) {
                int val = Math.abs(sudokuGridNum[ys][xs]);
                if (val == 0)
                    continue;
                possible.remove(val);
            }
        // Y line check
        for (int ys = 0; ys < 9; ys++) {
            int val = Math.abs(sudokuGridNum[ys][x]);
            if (val == 0)
                continue;
            possible.remove(val);
        }
        // X line check
        for (int xs = 0; xs < 9; xs++) {
            int val = Math.abs(sudokuGridNum[y][xs]);
            if (val == 0)
                continue;
            possible.remove(val);
        }

        for (int i = 1; i < 10; i++) {
            TextView view = numberButtonMap.get(i);
            if (!possible.contains(i))
                view.setVisibility(TextView.GONE);
            else
                view.setVisibility(TextView.VISIBLE);
        }

        if (sudokuGridNum[selectedCellY][selectedCellX] != 0)
            numberDelete.setVisibility(TextView.VISIBLE);
        else
            numberDelete.setVisibility(TextView.GONE);
        endGame();
    }

    private void setCell(final byte y, final byte x, final byte value) {
        sudokuGridNum[y][x] = (byte) -value;
        if (value != 0)
            sudokuGridText[y][x].setText(String.format(Locale.getDefault(), "%d", Math.abs(value)));
        else
            sudokuGridText[y][x].setText(" ");
    }

    private void loadSudokuGrid() {
        // Load save
        try (FileInputStream sudokuSave = openFileInput(SAVE_NAME)){
            byte[] byteArray = new byte[93];
            sudokuSave.read(byteArray, 0, 93);
            sudokuSave.close();
            this.deserialize(ByteBuffer.wrap(byteArray));

        }
        catch (IOException e) {
            this.sudokuGridNum = SudokuGenerator.generateRandomSudoku(this.currentDifficulty);
        }

        // Load all TextView
        TableLayout grid = findViewById(R.id.sudoku_grid);
        for (int r = 0; r < 3; r++) {
            TableRow row = (TableRow) grid.getChildAt(r);
            for (int c = 0; c < 3; c++) {
                TableLayout square = (TableLayout) row.getChildAt(c);
                for (int y = 0; y < 3; y++) {
                    TableRow sqRow = (TableRow) square.getChildAt(y);
                    for (int x = 0; x < 3; x++) {
                        TextView cell = (TextView) sqRow.getChildAt(x);
                        sudokuGridText[r * 3 + y][c * 3 + x] = cell;
                    }
                }
            }
        }

        // Set all sudoku values
        for (byte y = 0; y < 9; y++)
            for (byte x = 0; x < 9; x++) {
                setCell(y, x, (byte) -sudokuGridNum[y][x]);
                if (sudokuGridNum[y][x] > 0) {
                    sudokuGridText[y][x].setBackgroundResource(R.drawable.sudoku_cell_locked);
                    sudokuGridText[y][x].setTextColor(Color.WHITE);
                }

                final byte finalX = x;
                final byte finalY = y;
                sudokuGridText[y][x].setOnClickListener((view) ->
                        this.selectCell(finalY, finalX)
                );
            }

    }

    private void endGame() {
        for (byte y = 0; y < 9; y++)
            for (byte x = 0; x < 9; x++) {
                if (sudokuGridNum[y][x] == 0)
                    return;
            }
        simpleChronometer.stop();
        deleteSave(this);
        this.winDialog.show();
        long time = SystemClock.elapsedRealtime() - simpleChronometer.getBase();
        StatsActivity.updateScore(this, this.currentDifficulty.saveIndex, time);
    }

    private ByteBuffer serialize() {
        ByteBuffer saveBuffer = ByteBuffer.allocate(93);
        saveBuffer.putInt(this.currentDifficulty.saveIndex);
        long time = SystemClock.elapsedRealtime() - simpleChronometer.getBase();
        saveBuffer.putLong(time);
        for(int y = 0; y<9; y++) {
            for(int x = 0; x<9; x++) {
                saveBuffer.put(sudokuGridNum[y][x]);
            }
        }
        saveBuffer.flip();
        return saveBuffer;
    }

    private void deserialize(ByteBuffer saveBuffer) {
        int saveIndex = saveBuffer.getInt();
        switch(saveIndex) {
            case 0:
                this.currentDifficulty = SudokuDifficulty.EASY;
                break;
            case 1:
                this.currentDifficulty = SudokuDifficulty.MEDIUM;
                break;
            default:
                this.currentDifficulty = SudokuDifficulty.HARD;
                break;
        }
        long time = saveBuffer.getLong();
        long sec = (time/1000)%60;
        long min = (time/1000)/60;
        this.simpleChronometer.setBase(SystemClock.elapsedRealtime() - (min * 60000 + sec * 1000));

        for(int y = 0; y<9; y++) {
            for(int x = 0; x<9; x++) {
                sudokuGridNum[y][x] = saveBuffer.get();
            }
        }
    }
}
