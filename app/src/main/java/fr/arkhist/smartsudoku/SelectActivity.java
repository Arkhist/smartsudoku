package fr.arkhist.smartsudoku;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class SelectActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select);

        Button easyButton = this.findViewById(R.id.easyButton);
        easyButton.setOnClickListener(view -> {
            startGame(SudokuActivity.SudokuDifficulty.EASY);
        });

        Button mediumButton = this.findViewById(R.id.mediumButton);
        mediumButton.setOnClickListener(view -> {
            startGame(SudokuActivity.SudokuDifficulty.MEDIUM);
        });

        Button hardButton = this.findViewById(R.id.hardButton);
        hardButton.setOnClickListener(view -> {
            startGame(SudokuActivity.SudokuDifficulty.HARD);
        });
    }

    private void startGame(SudokuActivity.SudokuDifficulty difficulty) {
        hideButtons();
        Intent intent = new Intent(getApplicationContext(), SudokuActivity.class);
        intent.putExtra("level", difficulty.name());
        SudokuActivity.deleteSave(this);
        startActivity(intent);
        finish();
    }

    private void hideButtons() {
        this.findViewById(R.id.generatingMessage).setVisibility(View.VISIBLE);
        this.findViewById(R.id.easyButton).setVisibility(View.GONE);
        this.findViewById(R.id.mediumButton).setVisibility(View.GONE);
        this.findViewById(R.id.hardButton).setVisibility(View.GONE);
    }
}
