package fr.arkhist.smartsudoku.util;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class Util {
    public static void shuffleArray(int[] ar) {
        Random rnd = ThreadLocalRandom.current();
        for (int i = ar.length - 1; i > 0; i--) {
            int index = rnd.nextInt(i + 1);
            // Simple swap
            int a = ar[index];
            ar[index] = ar[i];
            ar[i] = a;
        }
    }

    public static int countNonZero(int[] ar) {
        int s = 0;
        for (int i = 0; i < ar.length; i++)
            if (ar[i] != 0)
                s++;
        return s;
    }

    public static boolean isZero(int[] ar) {
        for (int i = 0; i < ar.length; i++)
            if (ar[i] != 0)
                return false;
        return true;
    }

    public static int zeroArrPop(int[] ar) {
        for (int i = 0; i < ar.length; i++)
            if (ar[i] != 0) {
                int value = ar[i];
                ar[i] = 0;
                return value;
            }
        return 0;
    }
}
